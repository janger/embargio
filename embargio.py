#!/usr/bin/env python3
#
# main module
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys

abs_path = os.path.abspath(os.path.split(__file__)[0])
abs_modules = os.path.join(abs_path, "python")
abs_parse_modules = os.path.join(abs_modules, "parse_modules")
sys.path.insert(0, abs_path)
sys.path.insert(0, abs_modules)
sys.path.insert(0, abs_parse_modules)

from datetime import datetime
import optparse

import logger
# check python version
major = sys.version_info[0]
logger.debug("major python version: %s" % major)
if major != 3:
    logger.fatal("Must use python3!")

# check missing modules first
import check_dependencies

import configfileparser
from check import Check
from input_type import InputType
from source_data import SourceData
import functions

class Sanctions:
    def __init__(self, individuals=None, mixed=None, entities=None,
                 configfile="embargio.config",
                 without_user_interaction=False):
        """Start the sanctions check by reading the configuration
        file. Exits if the file is invalid.
        """
        self.without_user_interaction = without_user_interaction

        # create config object
        logger.debug("Using configuration file '%s'" % configfile)
        self.conf = configfileparser.parse(configfile, individuals, mixed,
                                           entities)

    def start_sanctions_check(self):
        """Starts to check the clients.
        First prints and checks the configuration.
        Then checks the given client sets.
        At last print and save the results.
        """
        self.confirm_config()

        # download and parse source data
        self.sd = SourceData(self.conf)

        try:
            self._create_check_objects()
        except KeyboardInterrupt:
            logger.fatal("\nInterruption: User has canceled the comparison")

        self.write_number_of_matches()

        # save matches as txt files
        self.save_matches()

    def _create_check_objects(self):
        """Creates a object of type Check foreach given compare file."""
        self.checks = []
        for key, filename in self.conf.filtered_clientfiles():
            print("Getting data: %s" % key)
            data = self.sd.format_data(key)
            self.checks += [Check(filename, key, data, self.conf)]

    def _exit_if_file_not_exists(self, filetype, filename):
        """Checks if a file given by name exists. If not prints an error
        message with filename and filetype (e.g. "persons") and exits
        with error code 1.
        """
        if not functions.file_exists(filename):
            msg = "The file %s (%s) is missing" % (filename, filetype)
            logger.fatal(msg)
    
    def confirm_config(self):
        """Asks the user, if the shown configuration is correct.
        Exits otherwise.
        """
        logger.debug("The user must confirm the configuration")
        print("")
        print(self.conf)
        if self.without_user_interaction:
            inp = "Y" 
        else:
            inp = self._get_input("Is the configuration correct? [Y,n] ")
            print("")
        config_is_correct = inp in ["Y", "Yes", "YES", "", "y", "yes"]
        if not config_is_correct:
            print("Please update the configuration file and start the program "
                  "again.")
            logger.debug("The user rejects the configuration. Exit program with"
                  " error code 2.")
            exit(2)
        logger.debug("Config has been confirmed by the user.")

    def write_number_of_matches(self):
        """Write a summary of the checks, with a number of matches per level"""
        print("\nSummary: ")
        for check in self.checks:
            print(check.format_summary())
        print("\n")

    def save_matches(self):
        """Writes the formatted results in an extern file."""
        #input_valid = False
        #while (not input_valid):
        #    self.conf.reload_config_file(self.filename)
        #    inp = self._get_input("Save results? [Y,n] ")
        #    input_valid = inp in ["Y", "Yes", "YES", "", "yes", "y",
        #                          "no", "No", "n"]

        date = datetime.now().strftime("%Y-%m-%d")

        for check in self.checks:
            name = "results-%s-%s.txt" % (check.inputtype, date)
            print("Save %s matches at %s" % (check.inputtype, name))
            summary = check.format_matches()
            folder = "results/"
            functions.create_folder(folder)
            r = functions.open_file(folder + name, mode='w')
            r.write(summary)
            r.close()
        print("Done.")

    def _get_input(self, message):
        """Asks a user a given massage and returns the input."""
        try:
            return input(">> " + message)
        except:
            print()
            msg = ("Interruption: User has interrupted data entry."
                  "\nExit with exit code 1.")
            logger.fatal(msg)

if __name__ == "__main__":
    # set and get options
    parser = optparse.OptionParser(description=__doc__)
    parser.add_option("-i", "--individual", dest="individuals",
                      metavar="FILE",
                      help="client file for individuals")

    parser.add_option("-e", "--entities", dest="entities",
                      metavar="FILE",
                      help="client file for entities")

    parser.add_option("-m", "--mixed", dest="mixed",
                      metavar="FILE",
                      help="client file for individuals and entities (mixed)")

    parser.add_option("-c", "--config", dest="configfile",
                      metavar="FILE", default="embargio.config",
                      help="alternative configuration file, default "
                      "is 'embargio.config'")

    (opts, args) = parser.parse_args()

    opts_dict = vars(opts)
    sanc = Sanctions(**opts_dict)
    sanc.start_sanctions_check()
