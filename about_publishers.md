# Checked publishers

## European Union
(about 2000 sanctioned individuals and entities)

The European Union offers different formats (XML, CSV, PDF, HMTL, RSS Feed):

- https://data.europa.eu/euodp/en/data/dataset/consolidated-list-of-persons-groups-and-entities-subject-to-eu-financial-sanctions

## IranWatch
(about 1000 sanctioned individuals and entities)

Iran Watch is a comprehensive website published by the Wisconsin Project on 
Nuclear Arms Control. The site tracks Iran’s ability to develop nuclear weapons,
ballistic missiles, and other weapons of mass destruction.

- https://www.iranwatch.org/iranian-entities
- https://www.iranwatch.org/suppliers

## Japan
(about 500 entities)

Japan offers an PDF file, with all sanctioned entities.
See METI (Ministry of Economy, Trade and Industry) for more details:

- https://www.meti.go.jp/english/policy/external_economy/trade_control/index.html
(paragraph End User List)

## United Kingdom
(about 2000 sanctioned entities and individuals)

The United Kingdom offers their consolidated list in different formats (HTML, CSV,
Excel, XLSX, XML and as a searching tool):

- https://www.gov.uk/government/publications/financial-sanctions-consolidated-list-of-targets/consolidated-list-of-targets

## United Nations
(about 1000 sanctioned individuals and entities)

The United Nations offer a searchable list of entities and individuals:

- https://www.un.org/securitycouncil/sanctions/narrative-summaries

## United States of America
(about 12000 sanctioned entitites and individuals)

The United States offer different sanctions list.

- Department of Commerce – Bureau of Industry and Security
    - Denied Persons List
    - Unverified List
    - Entity List

- Department of State – Bureau of International Security and Non-proliferation
    - Nonproliferation Sanctions

- Department of State – Directorate of Defense Trade Controls
    - AECA Debarred List

- Department of the Treasury – Office of Foreign Assets Control
    - Specially Designated Nationals List
    - Foreign Sanctions Evaders List
    - Sectoral Sanctions Identifications (SSI) List
    - Palestinian Legislative Council (PLC) List
    - Correspondent Account or Payable-Through Account Sanctions (CAPTA) List

More information about the different lists and the downloadable consolidated
screening list (in CSV, TSV and JSON):

- https://www.trade.gov/consolidated-screening-list

## Switzerland
(about 2000 sanctioned individuals and entities)

Switzerland offers an searchin tool that can be downloaded as Excel file:

- https://www.sesam.search.admin.ch/sesam-search-web/pages/search.xhtml
