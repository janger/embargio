# parses the configuration file
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#


import os
import csv
import configparser

import functions, config
from input_type import InputType
import logger

def _get_selected_publishers(config_publishers): 
    """Parses the publisher section.
    Returns a list of the full name of the selected publishers,
    e.g.: ['iranwatch', 'united states of america'].
    """
    publishers = []
    for publisher in functions.get_all_publishers(lower=True):
        in_config = publisher in config_publishers.keys()
        selected = not in_config or config_publishers.getboolean(publisher)
        if selected:
            # publisher is given as short form (e.g. 'eu')
            if publisher.lower() in functions.get_all_publishers(lower=True):
                publishers += [publisher]

            # publisher is given as full name (e.g. 'european union')
            elif publisher.lower() in functions.get_all_publishers(lower=True,
                                                               short_form=True):
                publishers += [functions.get_publisher_info(publisher,
                                                           only_full_name=True)]
    return publishers

def _get_encoding(config_clients):
    """Finds out the encodings of the given client files."""
    encoding = {"individuals": "utf-8", "mixed": "utf-8", "entities": "utf-8"}

    for key, value in config_clients.items():
        if value: # skip empty values
            enc = functions.get_encoding(value)
            encoding[key] = enc
    return encoding

def _get_client_files(config_clients, encoding):
    """Parses the given clients section.
    For every given file, check if the file exits and could be encoded.
    """
    clientfiles = {"individuals": None, "mixed": None, "entities": None}
    for key, item in config_clients.items():
        # skip encoding key and empty items
        if item == None or item == "":
            continue

        # key is valid?
        if key not in clientfiles.keys():
            msg = "The key '%s' is invalid in section 'clients'." % key
            logger.fatal(msg)

        # file is valid?
        enc = encoding[key]
        if _test_client_file(item, key, enc) == True:
            clientfiles[key] = item
            logger.debug("client file (%s): %s" % (key, item))

    return clientfiles

def _test_client_file(filename, filetype, encoding):
    """Tests if a file exists and could be encoded."""
    # check if the file exists
    if not functions.file_exists(filename):
        msg = ("Invalid configuration: The client file '%s' (%s) "
               "does not exist." % (filename, filetype))
        logger.warning(msg, print_msg=True)
        return False
    else:
    # check if the file could be encoded
        try:
            r = functions.open_file(filename, encoding)
            csv_reader = csv.reader(r, delimiter=",")
            x = [a for a in csv_reader]
            r.close()
        except UnicodeError as e:
            msg = ("Invalid encoding: The client file '%s' (%s) could not "
                   "be encoded." % (filename, filetype))
            logger.error(msg, error=e)
            return False
    return True


def _get_download_options(config_download):
    """Returns the both download options."""
    valid_options = ["use local files", "save download to"]

    save_to = None
    if "save download to" in config_download:
        save_to = config_download["save download to"]

    use_local_files = None
    if "use local files" in config_download:
        use_local_files = config_download.getboolean("use local files")

    # invalid keys?
    for key in config_download.keys():
        if key not in valid_options:
            msg = "The key '%s' is invalid in section 'download'" % key
            logger.fatal(msg)

    return use_local_files, save_to

def _get_compare_options(config_compare):
    """Returns the both compare options."""
    valid_options = ["comparison level", "min matching characters"]

    level = None
    if "comparison level" in config_compare:
        level = config_compare.getint("comparison level")

    min_chars = None
    if "min matching characters" in config_compare:
        min_chars = config_compare.getint("min matching characters")

    # invalid keys?
    for key in config_compare.keys():
        if key not in valid_options:
            msg = "The key '%s' is invalid in section 'compare'" % key
            logger.fatal(msg)

    return level, min_chars

def parse(filename, individuals=None, mixed=None, entities=None):
    """Parses the given configuration file.
    Preferably uses passes values. Returns an instance of the Config class.
    """
    # create config value object and parsing object
    if not functions.file_exists(filename):
        msg = "The configuration file '%s' does not exist" % filename
        logger.fatal(msg)
    config_values = config.Config()
    config_file = configparser.ConfigParser()

    # read config file
    try:
        config_file.read(filename)
    except configparser.ParsingError as e:
        msg = ("An error occurred while reading the configuration file:\n%s"
               % e.message)
        logger.fatal(msg)

    # test for unvalid sections
    valid_sections = ["publishers", "download", "clients", "compare"]
    for section in config_file.sections():
        if section not in valid_sections:
            msg = "The section '%s' is invalid." % section
            logger.warning(msg, print_msg=True)

    # read and test publisher informations
    if "publishers" in config_file.sections():
        selected_publishers = _get_selected_publishers(config_file["publishers"])
        config_values.selected_publishers = selected_publishers
        if len(selected_publishers) == 0:
            logger.fatal("Invalid configuration: No publishers were selected")

    # read and test client files
    if individuals != None or mixed != None or entities != None:
        logger.debug("Client files were passed as command line parameters, "
                     "so the files from the configuration file are ignored.")
        client_files = {"individuals": individuals, "mixed": mixed,
                        "entities": entities}

    elif "clients" in config_file.sections():
        client_files = config_file["clients"]

    else:
        client_files = {}

    # read and test client files
    encoding = _get_encoding(client_files)
    config_values.encoding = encoding

    config_values.clientfiles = _get_client_files(client_files, encoding)

    if list(filter(None, config_values.clientfiles.values())) == []:
        msg = "Invalid configuration: No valid client file is given"
        logger.fatal(msg)


    # read download options
    if "download" in config_file.sections():
        use_local_files, save_to = _get_download_options(config_file["download"])
        if use_local_files:
            config_values.use_local_files = use_local_files
        if save_to:
            config_values.save_download_to = save_to

    # read compare options
    if "compare" in config_file.sections():
        level, min_chars= _get_compare_options(config_file["compare"])
        if level:
            config_values.comparison_level = level
        if min_chars:
            config_values.min_matching_characters = min_chars

    logger.debug("%s" % config_values)

    return config_values

if __name__ == "__main__":
    parse("../sanctions.config")
