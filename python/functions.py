# frequently used functions and methods
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import shlex
import subprocess
import os
import shutil
import zipfile
import requests

import logger

def resort(name):
    """surname, givenname -> givenname surname"""
    l = name.split(",")
    l = [name.lower() for name in l]
    l.reverse()
    return " ".join(l)

def make_name(*names):
    """Returns 'name1 name2 name3 ...' as String, the given names a separated
    with a space and filters empty and None names."""
    l = list(filter(None, names))
    return " ".join(l)

def get_empty_dict():
    """Returns an empty persons dict with needed keys.
    Use this method to ensure that all parse modules return the same keys.
    """
    d = {}
    d["main name"] = ""
    d["names"] = []
    d["address"] = []
    d["link"] = ""
    d["program"] = ""
    d["ref id"] = ""
    return d

def download_file(url, save_to_file, save_to_path):
    """Downloads the given website using requests and saves under the
    given filename in the data folder.
    If the download fails, prints an error message and returns False.
    Otherwise returns True.
    """
    save_to = os.path.join(save_to_path, save_to_file)
    logger.debug("Download '%s' to file '%s'" % (url, save_to))

    proxies = {}

    if "HTTP_PROXY" in os.environ and "HTTPS_PROXY" in os.environ:
       proxies["http"] = os.environ["HTTP_PROXY"]
       proxies["https"] = os.environ["HTTPS_PROXY"]

    resp = None
    try:
       if proxies != {}:
            resp = requests.get(url, proxies=proxies)
       else:
            resp = requests.get(url)
       logger.debug("resp")
       resp.raise_for_status()
       logger.debug("raise")
       with open(save_to, "wb") as f:
           logger.debug("start write")
           f.write(resp.content)
           logger.debug("end write")
    except Exception as e:
        msg = "Failed to download file: '%s'" % save_to_file
        logger.error(msg, e)
        return False
    finally:
       if not resp == None:
            resp.close()

    return True

def download_and_unzip_excel(url, filename, filepath):
    """Downloads an excel file using download_file and unzips it to make
    it easier to read.
    Returns False, if something goes wrong.
    Otherwise returns True.
    """
    succ = download_file(url, filename, filepath)
    if succ == False:
        return False

    full_filename = os.path.join(filepath, filename)

    try:
        zf = zipfile.ZipFile(full_filename, "r")
        zf.extractall(filepath)
        zf.close()
    except FileNotFoundError as e:
        msg = ("The file '%s' does not exists. Please download again."
                    % full_filename)
        logger.error(msg, e)
        return False
    except zipfile.BadZipFile as e:
        msg = "The file '%s' is invalid. Please download again." % full_filename
        logger.error(msg, e)
        return False

    # delete unneeded files and folders
    delete_folder_or_file(os.path.join(filepath, "docProps"))
    delete_folder_or_file(os.path.join(filepath, "_rels"))
    delete_folder_or_file(os.path.join(filepath, "[Content_Types].xml"))

    return True

def open_file(filename, encoding=None, mode="r"):
    """Opens a given filename.
    Use read only mode and default encoding (UTF-8), if not specified.
    Returns the TextIOWrapper.
    If an error occurs, prints the error message.
    """
    try:
        if encoding:
            return open(filename, mode, encoding=encoding)
        return open(filename, mode)
    except OSError as e:
        msg = "Failed to open file '%s'" % filename
        logger.error(msg, e)

def file_exists(name, prefix=""):
    """Check wheter a file with given name exists.
    Use prefix= to check if the file exists in the given folder.
    """
    name = os.path.join(prefix, name)
    return os.path.isfile(name)

def delete_folder_or_file(path):
    """Deletes the given file or folder.
    If the folder contains other files, this files will be deleted too.
    """
    if os.path.isfile(path):
        os.remove(path)
        logger.debug("File deleted: %s" % path)
    elif os.path.isdir(path):
        shutil.rmtree(path)
        logger.debug("Folder deleted: %s" % path)
    else:
        logger.warning("Failed to delete'%s': given path is no file or folder")

def create_folder(folder, data_folder=None):
    """Creates a new folder with given name using 'os.mkdir'.
    Use data_folder=<name> to create the folder in the given folder.
    It is possible to pass a folder name with multiple folders, like 'a/b/c',
    this would create 'a', 'a/b' and 'a/b/c '.
    """
    logger.debug("Create folder: %s" % folder)
    path = folder

    # create data folder if needed
    if data_folder:
        logger.debug("Create data folder: %s" % data_folder)

        # use sub folders to create the data folder path folder by folder
        for sub_folder in _get_subfolders(data_folder):
            if not os.path.exists(sub_folder):
                os.mkdir(sub_folder)
        path = os.path.join(data_folder, path)

    # create folder if needed
    # use sub folders to create the folder path folder by folder
    for sub_folder in _get_subfolders(path):
        if not os.path.exists(sub_folder):
            os.mkdir(sub_folder)
        logger.debug("created folder: %s" % folder)
    else:
        logger.debug("folder '%s' already exists" % folder)

def _get_subfolders(path):
    """Returns all subfolders of a given path.
    E.g.: path = 'dir/subdir/subdir2/folder' will return
    ['dir', 'dir/subdir', 'dir/subdir/subdir2', 'dir/subdir/subdir2/folder']
    """
    res = []
    while path != "":
        res.insert(0, path)
        path = os.path.split(path)[0]
    return res

def get_publisher_info(short, only_full_name=False):
    """Returns the fullname and the short form name of an given
    publisher in the format '<fullname> (<short name>)'.
    If only_full_names=True, returns '<fullname>'.
    Prints an warning, if the given publisher is invalid.
    """
    short = short.lower()
    short_list = ["ch", "eu", "iw", "jp", "uk", "un", "us"]
    try:
        index = short_list.index(short)
    except ValueError:
        msg = ("The publisher '%s' has no informations."
              % short)
        logger.warning(msg, print_msg=True)
        return ""

    if only_full_name:
        return "%s" % get_all_publishers()[index]

    return "%s (%s)" % (get_all_publishers()[index], short)

def get_all_publishers(lower=False, short_form=False):
    """Returns a list of all known publishers.
    If lower=True, returns a list with the lowercase names.
    If short_form=True, returns a list with the short forms.
    """
    publishers = []
    if short_form:
        publishers = ["CH", "EU", "IW", "JP", "UK", "UN", "US"]
    else:
        publishers = ["Switzerland",
                      "European Union",
                      "IranWatch",
                      "Japan",
                      "United Kingdom",
                      "United Nations",
                      "United States of America"]

    if not lower:
        return publishers

    return [publisher.lower() for publisher in publishers]

def get_encoding(filename):
    """Find out the encoding of a given file.
    Returns the encoding as string.
    If an error occurs, the default value "utf-8" is returned.
    """
    command_line = "file -bi %s" % filename
    logger.debug("Execute command: '%s'" % command_line)
    args = shlex.split(command_line)
    try: 
        p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except OSError as e:
        msg = "The program 'file' is not installed."
        logger.error(msg, error=e)
        exit(3)

    # wait until the process has finished
    p.wait()
    stdout = p.stdout.read().decode("utf-8")
    logger.debug("Standard Output: %s" % stdout)
    p.stdout.close()
    stderr = p.stderr.read().decode("utf-8")
    logger.debug("Standard Error: %s" % stderr)
    p.stderr.close()

    p.terminate()

    parts = stdout.split("; charset=")
    if len(parts) != 2:
        logger.warning("Unable to find encoding of '%s'." % filename,
                       print_msg=True)
        return "utf-8"

    encoding = parts[1]
    encoding = encoding.replace("\n", "") # remove final \n
    return encoding
