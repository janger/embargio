# check client files for compliance with sanction lists
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import sys
import csv

import functions
import logger

class Check:
    """Reads the list of persons/entities to check, given by filename.
    Compares every entry in this list with all sanctioned
    persons/entities/mixed.
    """

    def __init__(self, inputfile, inputtype, source_data, conf):
        """Loads the sanctioned persons/entities from the source path
        and compares them with the persons/entities from the given
        inputfile.
        The inputfile is the name of the file, inputtype should be a
        InputType.
        """
        self.inputfile = inputfile
        self.inputtype = inputtype
        self.data = source_data
        self.level = conf.comparison_level
        self.min_characters = conf.min_matching_characters
        self.level_values = ["", "perfect", "high", "moderate-high",
                             "moderate-weak", "weak"]
        self.encoding = conf.encoding[inputtype]
        logger.debug("Start Check with:\ninputfile: %s\ninputtype: %s"
                      % (inputfile, inputtype))

        self._compare()

    def _count_entries(self, filename):
        """Returns the number of entries in the given csv file.
        If the file can not be opened, returns -1.
        """
        r = functions.open_file(filename, mode="rb")
        if r == None:
            return -1
        c_lines = sum(1 for row in r.readlines())
        r.close()
        return c_lines

    def _compare(self):
        """Reads the client csv and compare with the sanctioned
        persons/groups. The matches are stored in self.matches.
        """
        self.matches = {}
        r = functions.open_file(self.inputfile, encoding=self.encoding)
        if r == None:
            msg = "The input file with %s was skipped." % self.inputtype
            logger.warning(msg, print_msg=True)
            return -1

        csv_reader = csv.reader(r, delimiter=",")

        n_clients = self._count_entries(self.inputfile)
        if n_clients == -1:
            logger.warning("Failed to count clients")
            n_clients = 0
            write_progress = False
        else:
            logger.debug("Number of clients: %s" % n_clients)
            write_progress = True

        if write_progress:
            len_n_clients = len(str(n_clients))
            ceros = "0" * len_n_clients
            back = "\b" * len_n_clients
            sys.stdout.write("compared clients (%s): %s / %d\b\b\b%s"
                    % (self.inputtype, ceros, n_clients, back))

        for obj in csv_reader:
            # surname and given name of individual or name of entity
            name = " ".join(obj)
            self.matches[name] = self._get_name_matches(name) 
            if write_progress:
                infoline = "%s%0*d" % (back, len_n_clients, csv_reader.line_num)
                sys.stdout.write(infoline)
                sys.stdout.flush()
        r.close()

        print("")

    def _get_name_matches(self, clientname):
        """Requires the whole name from a client.
        Tests, if the name of the client matchs a name from a
        sanctioner. Every match has an agreement value, that says
        how exact the match is. (nameparts client <> nameparts sanctioner)
        - 1: perfect match, the whole names are the same (A B C <> A B C)
        - 2: the sanctionername contains every namepart from
             client (A B <> A B C)
        - 3: the clientname contains every namepart from
             sanctioner (A B C <> A B)
        - 4: client and sanctioner have at least two nameparts in
             common (A B C <> D A C)
        - 5: client and sanctioner have the same last name (A B C <> D E C)
        Returns a dict, that contains a list for every value, the lists contain
        reference numbers.
        """
        d = {1: [], 2: [], 3: [], 4: [], 5: []}

        for key, sanctioner in self.data.items():
            if clientname == "" or sanctioner["names"] == []:
                continue

            # some sources use upper case to mark the surname
            clientname = clientname.lower()
            sanctionernames = [e.lower() for e in sanctioner["names"]]
            sanctionernames = [functions.resort(e) for e in sanctionernames]
            sanctionernames = [e.strip() for e in sanctionernames]


            type_nr, matchname = self._compare_two_names(clientname,
                                                         sanctionernames)
            if matchname:
                # add original names (Case sensitiv)
                index = sanctionernames.index(matchname)
                match_name = sanctioner["names"][index]
                main_name = sanctioner["main name"]
                if main_name != match_name:
                    name_info = "%s (Alias: %s)" % (main_name, match_name)
                else:
                    name_info = match_name
                d[type_nr] += [(key, name_info)]
        return d

    def _compare_two_names(self, clientname, sanctionernames):
        # type 1: perfect match, the whole names are the same
        # A B C <> A B C
        if clientname in sanctionernames:
            return 1, clientname
        if self.level == 1:
            return 0, False

        # type 2: the sanctionername contains every namepart from client
        # A B <> A B C
        for sanctionername in sanctionernames:
            for namepart in clientname.split(" "):
                if namepart not in sanctionername.split(" "):
                    break
            else: # if 'break' was not called
                if len(sanctionername) >= self.min_characters:
                    return 2, sanctionername
        if self.level == 2:
            return 0, False

        # type 3: the clientname contains every namepart from sanctioner
        # A B C <> A B
        for sanctionername in sanctionernames:
            for namepart in sanctionername.split(" "):
                if namepart not in clientname.split(" "):
                    break
            else:
                if len(sanctionername) >= self.min_characters:
                    return 3, sanctionername
        if self.level == 3:
            return 0, False

        # type 4: client and sanctioner have at least two nameparts in common
        # A B C <> D A C
        for sanctionername in sanctionernames:
            client_nameparts = clientname.split(" ")
            sanctioner_nameparts = sanctionername.split(" ")
            common = set(client_nameparts).intersection(sanctioner_nameparts)
            if len(common) >= 2 and len(sanctionername) >= self.min_characters:
                return 4, sanctionername
        if self.level == 4:
            return 0, False

        # type 5: client and sanctioner have the same last name (A B C <> D E C)
        sanctioner_surnames = [x.split(" ")[-1] for x in sanctionernames]
        client_surname = clientname.split(" ")[-1]
        if client_surname in sanctioner_surnames:
            return 5, sanctionernames[sanctioner_surnames.index(client_surname)]

        # no matches
        return 0, False

    def format_matches(self):
        """Formats a list of all hits, sorted by names."""
        text = ""
        for name in self.matches.keys():
            text += self._format_match_by_name(name)
        return text

    def format_summary(self):
        """Returns a summary of the checks, with a number of matches per
        level.
        """
        logger.debug("Start formatting the summary")
        summary = "\n   %s:" % self.inputtype
        for l in range(1, self.level+1):
            c = 0
            for match in self.matches.values():
                c += len(match[l])
            summary += ("\n      %s match (level %d): %d"
                        % (self.level_values[l], l, c))

        logger.debug("Summary formatted")
        return summary

    def _format_match_by_name(self, name):
        """Formats a string representation of the matches of the given
        clientname.
        """
        output = ""
        empty = self.matches[name] == {1: [], 2: [], 3: [], 4: [], 5: []}
        if empty:
            return ""

        output += name + "\n"

        for agree, l in self.matches[name].items():
            # print summary
            if len(l) != 0:
                output += "   %2s hit(s) with %s match (level %d)\n" \
                            % (len(l), self.level_values[agree], agree)

                # print details
                for key, value in l:
                    publisher_short = key.split(".")[0]
                    publisher = functions.get_publisher_info(publisher_short)

                    output += "      >  %s\n" % value
                    output += "           - publisher: %s\n" % publisher
                    output += "           - link: %s\n" % self.data[key]["link"]
                    if  self.data[key]["program"] != "":
                        output += ("           - program/regime: %s\n"
                                    % self.data[key]["program"])
                    if "ref id" in self.data[key]:
                        output += ("           - reference number: %s\n"
                                    % self.data[key]["ref id"])
                output += "\n"
        output += "\n"
        return output

if __name__ == "__main__":
    s = Check()
