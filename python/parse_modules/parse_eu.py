# download and parse sanction list(s) from the European Union
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import json
import csv
import os

import functions
import logger

"""
Publisher information:
The file contains individuals and entities.
Every line contains each attribute one or zero times.
If more than one value per attribute is given, one person
has more lines.
The first line is the header with description of every field.

The dicts have the reference numbers as keys, e.g. EU.5581.16
"""

filename = os.path.join("eu", "eu_all.csv")

def parse(data_folder):
    """The csv file contains many values. The name, the type, the reference
    number and some location attributes are used.
    The file contains individuals and entities, a type attribute is present.
    """
    logger.info("Start parsing European Union")

    if not functions.file_exists(filename, prefix=data_folder):
        print("WARNING: European Union skipped because file(s) are missing. "
              "Please download it again.")
        logger.warning("European Union skipped, the file %s does not exist." %
                       filename)
        return {}, {}, {}

    print("Parsing EU file...")
    filepath = os.path.join(data_folder, filename)
    r = functions.open_file(filepath)
    if r == None:
        return
    csv_reader = csv.reader(r, delimiter=";")

    individuals = {}
    entities = {}
    mixed = {}

    try:
        header = next(csv_reader)
    except StopIteration:
        msg = "The downloaded file is empty. Please download it again."
        logger.fatal(msg)

    try:
        nr_link = header.index("Leba_url") # or Naal_leba_url?
        nr_type = header.index("Subject_type")
        nr_ref = header.index("EU_ref_num")
        nr_name = header.index("Naal_wholename")
        nr_program = header.index("Programme")
        #nr_city = header.index("Addr_city")
        #nr_country = header.index("Addr_country")
        #nr_citizen = header.index("Citi_country")
    except ValueError as e:
        msg = ("The structure of the EU sanction has changed. At least "
              "one key is no longer valid or present.")
        logger.error(msg, error=e)
        return {}, {}, {}

    # collect date off all individuals
    data = {}
    for line in csv_reader:
        ref_num = line[nr_ref]
        if ref_num not in data:
            data[ref_num] = {"names": [],
                             "types": [],
                             "programs": [],
                             "links": []}

        # add data to sorted dict without duplications
        name = line[nr_name] # "Naal_wholename"
        if name != "" and name not in data[ref_num]["names"]:
            data[ref_num]["names"] += [name]
        ty = line[nr_type]

        if ty != "" and ty not in data[ref_num]["types"]:
            data[ref_num]["types"] += [ty]

        link = line[nr_link]
        if link != "" and link not in data[ref_num]["links"]:
            data[ref_num]["links"] += [link]

        program = line[nr_program]
        if program != "" and program not in data[ref_num]["programs"]:
            data[ref_num]["programs"] += [program]

    # connect entries with same ref
    nr = 0
    for ref_num, item in data.items():
        nr += 1
        key = "EU.%03d" % nr

        # check format
        if (len(item["types"]) != 1 or len(item["names"]) == 0):
            logger.warning("The EU sanction object with the reference "
                  "number %s has an incorrect format." % ref_num)
            continue

        d = functions.get_empty_dict()
        d["main name"] = item["names"][0]
        d["names"] = item["names"]
        d["ref id"] = ref_num
        d["program"] = ", ".join(item["programs"])
        d["link"] = "\n".join(item["links"])

        if item["types"] == ["P"]:  # P: individuals, person
            individuals[key] = d
        elif item["types"] == ["E"]: # E: entities, groups
            entities[key] = d
        else:
            mixed[key] = d

    print("File successfully parsed:")
    logger.debug("EU files parsed successfully with %d individuals, %s entities "
                 "and %d mixed." % (len(individuals), len(entities), len(mixed)))

    r.close()
    return individuals, entities, mixed

def download(data_folder):
    """Downloads the EU sanction list."""
    logger.info("Starting downloading EU files.")
    print("Downloading EU file...")

    functions.create_folder("eu", data_folder=data_folder)

    # use api to load dict
    url = ("https://webgate.ec.europa.eu/europeaid/fsd/fsf/public/files/"
           "csvFullSanctionsList/content?token=dG9rZW4tMjAxNw")
    succ = functions.download_file(url, filename, data_folder)

    if succ:
        print("File successfully downloaded.")
        logger.info("File successfully downloaded to %s" % filename)
    return succ

