# download and parse sanction list(s) from the United Kingdom
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import csv
import os

import functions
import logger

"""
Publisher information:
The UK offers an CSV file. The first line contains the last updated date,
the second line is the header.
The first columns are name parts, 'Name 6' is the surname and always given and
Name 1 to 5 are additional given names.
Also a reference number and the type are given.

Every line contains each attribute one or zero times. If a individuals has
an attribute more than one time, more lines a given for the person.

The download path is static and does not depend on any other information,
such as year.
"""

filename = os.path.join("uk", "hm_list.csv")

def parse(data_folder):
    """The csv file contains many values. The name, the type, the reference
    number and the country are used.
    The file contains individuals and entities, a type attribute is given.
    """
    logger.info("Start parsing United Kingdom")
    
    # check if file exists
    logger.info("Checking file %s" % filename)
    if not functions.file_exists(filename, prefix=data_folder):
        print("WARNING: United Kingdom skipped because file(s) are missing. "
              "Please download again.")
        logger.warning("United Kingdom skipped, the file %s does not exist."
                        % filename)
        return {}, {}, {}
    logger.debug("File exist")

    filepath = os.path.join(data_folder, filename)
    print("Parsing United Kingdom files...")
    r = functions.open_file(filepath, encoding="latin-1")
    if r == None:
        return
    csv_reader = csv.reader(r, delimiter=",")

    individuals = {}
    entities = {}
    mixed = {}

    data = _reader_to_individuals(csv_reader)

    # connect entries with same ref
    nr = 0
    for ref_num, item in data.items():
        nr += 1
        key = "UK.%03d" % nr

        # check format
        if (len(item["types"]) != 1 or len(item["names"]) == 0):
            msg = ("The UK sanction object with the reference "
                  "number %s has an incorrect format." % ref_num)
            logger.warning(msg, print_msg=True)
            continue

        d = functions.get_empty_dict()
        d["main name"] = item["names"][0]
        d["names"] = item["names"]
        d["ref id"] = ref_num
        d["program"] = ", ".join(item["programs"])
        d["link"] = ("https://www.gov.uk/government/publications/"
                     "financial-sanctions-consolidated-list-of-targets/"
                     "consolidated-list-of-targets")

        if item["types"] == ["Individual"]:
            individuals[key] = d
        elif item["types"] == ["Entity"]:
            entities[key] = d
        else:
            mixed[key] = d

    print("File successfully parsed:")
    logger.debug("United Kingdom files parsed successfully with %d individuals, "
                 "%s entities and %d mixed." % (len(individuals), len(entities),
                     len(mixed)))
    r.close()
    return individuals, entities, mixed

def _reader_to_individuals(reader):
    """Consolidates lines with the same reference number.
    Returns a dict, with the reference number as key and lists
    containing names, types and links as values.
    """
    # first line: last updated; second line: header
    next(reader)
    header = next(reader)
    try:
        nr_surname = header.index("Name 6")
        nr_first_given_name = header.index("Name 1")
        nr_last_given_name = header.index("Name 5")
        nr_type = header.index("Group Type")
        nr_ref = header.index("Group ID")
        nr_program = header.index("Regime")
        #nr_city = header.index("Addr_city")
        #nr_country = header.index("Addr_country")
        #nr_citizen = header.index("Citi_country")
    except ValueError as e:
        msg = ("The structure of the UK sanction has changed. At least "
              "one key is not longer available. "
              "Please contact the development.")
        logger.error(msg, error=e)
        return {}, {}, {}

    # consolidate lines
    data = {}
    for line in reader:
        ref_num = line[nr_ref]
        if ref_num not in data:
            data[ref_num] = {"names": [],
                             "types": [],
                             "programs": [],
                             "links": []}

        # connect not empty (None) name parts
        names = filter(None, line[nr_first_given_name : nr_last_given_name+1]
                         + [line[nr_surname]])
        name = " ".join(names)
        if name != "" and name not in data[ref_num]["names"]:
            data[ref_num]["names"] += [name]

        ty = line[nr_type]
        if ty != "" and ty not in data[ref_num]["types"]:
            data[ref_num]["types"] += [ty]

        program = line[nr_program]
        if program != "" and program not in data[ref_num]["programs"]:
            data[ref_num]["programs"] += [program]

    return data

def download(data_folder):
    """Downloads the CSV file using the functions download method."""
    logger.info("Starting downloading UK files.")
    print("Downloading UK file...")
    functions.create_folder("uk", data_folder=data_folder)

    # download and save dict
    url = "https://ofsistorage.blob.core.windows.net/publishlive/2022format/ConList.csv"
    succ = functions.download_file(url, filename, data_folder)

    if succ:
        print("File successfully downloaded.")
        logger.info("File succesfully downloaded to %s" % filename)
    return succ

