# download and parse sanction list(s) from the United States
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import csv
import os

import functions
import logger

"""
Publisher information:
The USA offer a Consolidated Screening List, a CSV file containing all
sanction programs.
Every entry has a link for more informations.
Depending of the program, a type is given or not.
See https://www.trade.gov/consolidated-screening-list for a detailed
overview about the different programs and a searching tool.
"""

filename = os.path.join("us", "us_all.csv")

def parse(data_folder):
    """All US lists are part of this csv file.
    The csv contains many values. The type, the program, the name, address
    and link are used."""
    logger.info("Start parsing USA")

    # check if file exits
    logger.debug("Checking file %s" % filename)
    if not functions.file_exists(filename, prefix=data_folder):
        print("WARNING: United States skipped because file(s) are missing. "
              "Please download again.")
        logger.warning("United States skipped, the file %s does not exist."
                        % filename)
        return {}, {}, {}
    logger.debug("File exists")

    print("Parsing US files...")
    nr = 0
    individuals = {}
    entities = {}
    mixed = {}
    
    filepath = os.path.join(data_folder, filename)
    r = functions.open_file(filepath)
    if r == None:
        return
    csv_reader = csv.reader(r, delimiter=",")

    # first line: header
    try:
        header = next(csv_reader)
    except StopIteration:
        msg = "The downloaded US file is empty. Please download again."
        logger.error(msg)
        return {}, {}, {}

    try:
        nr_link = header.index("source_information_url")
        nr_source = header.index("source")
        nr_program = header.index("programs")
        nr_type = header.index("type")
        nr_ref = header.index("federal_register_notice")
        nr_ref = header.index("_id")
        nr_name = header.index("name")
        nr_alt_names = header.index("alt_names")
        #nr_addresses = header.index("addresses")
    except ValueError as e:
        msg = ("The structure of the US sanction has changed. At least "
               "one key is not longer available. "
               "Please contact the development.")
        logger.error(msg, e)
        return {}, {}, {}
    

    # consolidate lines
    nr = 0
    for line in csv_reader:
        nr += 1
        key = "US.%03d" % nr
        d = functions.get_empty_dict()

        # alternative names in line e.g. are
        # "SURNAME, given1 given2; SURNAME, given1"
        alt_names = [functions.resort(name)
                     for name in line[nr_alt_names].split(";")]
        main_name = functions.resort(line[nr_name])
        for name in [main_name] + alt_names:
            if name != "" and name not in d["names"]:
                d["names"] += [name]
        d["main name"] = main_name

        # link and source program
        if d["link"] != "":
            d["link"] += "\n"
        d["link"] += line[nr_link]
        d["program"] = "%s [%s]" % (line[nr_source], line[nr_program])

        if (line[nr_type] == "Individual"
                or "Denied Persons List" in line[nr_source]):
            individuals[key] = d
        elif (line[nr_type] == "Entity"
                or "Entity List" in line[nr_source]):
            entities[key] = d
        elif line[nr_type] == "Vessel" or line[nr_type] == "Aircraft":
            continue
        else:
            mixed[key] = d

    print("File successfilly parsed:")
    logger.debug("USA files parsed successfully with %d individuals, %s "
                 "entities and %d mixed." % (len(individuals), len(entities),
                    len(mixed)))
    r.close()
    return individuals, entities, mixed

def download(data_folder):
    """Downloads the csv file using the functions download method."""
    logger.info("Starting downloading USA file.")
    print("Downloading US file...")
    functions.create_folder("us", data_folder=data_folder)

    # download and save the csv
    url = ("http://api.trade.gov/static/consolidated_screening_list/"
           "consolidated.csv")
    succ = functions.download_file(url, filename, data_folder)

    if succ:
        print("File successfully downloaded.")
        logger.info("File successfully downloaded to %s" % filename)
    return succ

