# download and parse sanction list(s) from IranWatch
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import os

import functions
import logger

"""
Publisher information:

Iran Watch Suppliers:   https://www.iranwatch.org/suppliers/
Iran Watch Entities:    https://www.iranwatch.org/irian-entities

There is no list which contains informations about the individuals/entities.
Every person/entity have a single html page under the named subdirectory.
The download gets only the html pages that list the suppliers/entities and
parses the list.
"""

ENTITIES = "iranian-entities"
SUPPLIERS = "suppliers"

def parse(data_folder):
    """In the <sanctions>/iw/ folder are about 45 files with suppliers and
    entities. Each file contains a maximum of 25 suppliers/entities.
    """
    logger.info("Start parsing IranWatch.")

    # check if first file exists
    first_filename = os.path.join("iw", "suppliers00")
    logger.debug("Checking the first file %s" % first_filename)
    if not functions.file_exists(first_filename, prefix=data_folder):
        print("WARNING: IranWatch skipped because file(s) are missing. "
              "Please download again.")

        logger.warning("IranWatch skipped, the file '%s' does not exist."
                        % first_filename)
        return {}, {}, {}
    logger.debug("File exists")

    print("Parsing IranWatch files...")
    mixed = {}
    nr = 0
    sancfolder = data_folder
    for filename in os.listdir(os.path.join(sancfolder, "iw")):
        filepath = os.path.join(sancfolder, "iw", filename)
        r = functions.open_file(filepath)
        if r == None:
            return

        searchstr = "<div class=\"views-field views-field-title\">"
        endstr = "Go to last page"

        for line in r.readlines():
            if searchstr in line:
                nr += 1
                key = "IW.%03d" % nr
                mixed[key] = line_to_person(line)
            if endstr in line:
                break
        r.close()
    
    print("Files successfully parsed:")
    logger.debug("IranWatch files parsed successfully with 0 individuals, 0 "
                 "entities and %d mixed." % len(mixed))
    return {}, {}, mixed
         
def line_to_person(line):
    """The lines have, typical for hyperlinks html, first the url and then 
    the displayed text. The displayed text is the name of the person and the
    link is the link to a more detailed page about the person.
    """
    link_prefix = "https://www.iranwatch.org"
    link_startindex = line.index("href") + 6
    link_endindex   = line.index(">", link_startindex) - 1
    name_startindex = link_endindex + 2
    name_endindex   = line.index("<", name_startindex)
    
    d = functions.get_empty_dict()
    d["names"] = [line[name_startindex:name_endindex]]
    d["main name"] = line[name_startindex:name_endindex]
    d["link"] = link_prefix + line[link_startindex:link_endindex]
    if "suppliers" in d["link"]:
        d["program"] = "Suppliers"
    elif "entities" in d["link"]:
        d["program"] = "Iranian Entities"

    return d

def download_list(sublist, data_folder):
    """Requires SUPPLIERS ("suppliers") or ENTITIES ("irian-entities") as sublist.
    Downloads only the pages that list the names of the entities/suppliers.
    """
    nr = 0
    needed_files = 1

    while nr <= needed_files:
        filename = os.path.join("iw", "%s%02d" % (sublist, nr))
        url = "https://www.iranwatch.org/%s?page=%d" % (sublist, nr)
        succ = functions.download_file(url, filename, data_folder)
        if succ == False:
            return False

        if nr == 0:
            needed_files = get_needed_files(sublist, data_folder)
            logger.debug("Number of files for '%s': %s" % (sublist, needed_files))
        nr += 1
    return True

def download(data_folder):
    """Downloads the IranWatch sanction list."""
    logger.info("Start downloading IranWatch files")
    print("Downloading IranWatch files (this will take a moment)...")
    functions.create_folder("iw", data_folder=data_folder)

    logger.info("Start downloading '%s' files" % SUPPLIERS)
    succ_suppliers = download_list(SUPPLIERS, data_folder)
    if succ_suppliers == False:
        return False

    logger.info("Start downloading '%s' files" % ENTITIES)
    succ_entities = download_list(ENTITIES, data_folder)
    if succ_entities == False:
        return False

    print("Files successfully downloaded.")
    logger.info("Files successfully downloaded to ./iw/*")
    return True


def get_needed_files(sublist, data_folder):
    """The number of needed files depends on the number of entries.
    Therefore use the first file and find the link to "go to last page",
    this link contains the number of the last page.
    """
    filepath = os.path.join(data_folder, "iw", "%s00" % sublist)
    r = functions.open_file(filepath)
    text = r.read()
    r.close()
    startindex = text.index("Go to last page")
    index = text.index("page=", startindex)
    try:
        return int(text[index+5:index+7])
    except:
        if sub == ENTITIES:
            return 40
        else:
            return 15
