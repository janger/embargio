# download and parse sanction list(s) from the United Nations
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import xmltodict
import os

import functions
import logger

"""
Publisher information:

The UN offers the following sanctions lists:

REFERENCE_NUMBER | UN_LIST_TYPE | Whole name
            SO      Somalia         Somalia Sanctions Regime
            QD      Al_Qaida        ISIL (Da’esh) and Al-Qaida Sanctions Regime
            IQ      Iraq            Iraq Sanctions Regime
            CD      DRC             Democratic Republic of the Congo (DRC) Sanctions Regime
            Sd      Sudan           Sudan Sanctions Regime
            IR      Iran            1636 Sanctions Regime
            KP      DPRK            Democratic People’s Republic of Korea (DPRK) Sanctions Regime
            LY      Libya           Libya  Sanctions  Regime
            TA      Taliban         1988  Sanctions  Regime
            GB      GB              Guinea-Bissau Sanctions Regime
            CF      CAR             Central African Republic (CAR) Sanctions Regime
            YE      Yemen           Yemen Sanctions Regime
            SS      SouthSudan      South Sudan Sanctions Regime
            ML      Mali            Mali Sanctions Regime

All lists are in one xml file.
The individuals and entities are separated.
The xml has first the key INDIVIDUALS with the subkey INDIVIDUAL,
then has the key ENTITIES with subkey ENTITY.
"""

filename = os.path.join("un", "un_csl.xml")

def parse(data_folder):
    """Open sourcefile and parse it to dict, using xmltodict.
    The xml is divided in 'INDIVIDUALS' and 'ENTITIES', so
    its possible to separate them."""
    logger.info("Start parsing United Nations")

    # check if file exits
    logger.debug("Checking file %s" % filename)
    if not functions.file_exists(filename, prefix=data_folder):
        print("WARNING: United Nations skipped, missing file(s). "
              "Please download again.")
        logger.warning("United Nations skipped, the file %s does not exist."
                        % filename)
        return {}, {}, {}

    print("Parsing UN file...")
    individuals = {}
    entities = {}
    mixed = {}

    filepath = os.path.join(data_folder, filename)
    r = functions.open_file(filepath)
    if r == None:
        return

    try:
        source_dict = xmltodict.parse(r.read(), postprocessor=postprocessor,
                                   force_list=("ALIAS", "INDIVIDUAL", "ENTITY"))
    except xml.parsers.expat.ExpatError as e:
        msg = ("The file of the UN sanctions has an incorrect format. "
               "Please download the file again and try again.")
        logger.error(msg, error=e)
        return {}, {}, {}
    finally:
        r.close()

    link = "https://www.un.org/securitycouncil/sanctions/narrative-summaries"
    links = {}
    links["INDIVIDUAL"] = ("https://www.interpol.int/en/How-we-work/"
                        "Notices/View-UN-Notices-Individuals")
    links["ENTITY"] = ("https://www.interpol.int/en/How-we-work/"
                     "Notices/View-UN-Notices-Entities")

    nr = 0
    for item in source_dict["LIST"]:
        nr += 1
        key = "UN.%03d" % nr
        d = functions.get_empty_dict()
        ty = item["TYPE"]
        d["link"] = link + ", " + links[ty]
        d["ref id"] = item["REFERENCE_NUMBER"]
        d["program"] = item["UN_LIST_TYPE"]

        # add real name and alias names 
        name = functions.make_name(item["FIRST_NAME"], item["SECOND_NAME"],
                                   item["THIRD_NAME"])
        d["main name"] = name
        d["names"] += [name] + item["ALIAS"]

        if item["TYPE"] == "ENTITY":
            entities[key] = d
        elif item["TYPE"] == "INDIVIDUAL":
            individuals[key] = d
        else:
            mixed[key] = d

    print("File successfilly parsed:")
    logger.debug("United Nations files parsed successfully with %d individuals,"
                 " %s entities and %d mixed." % (len(individuals),
                len(entities), len(mixed)))

    return individuals, entities, mixed


def postprocessor(path, key, value):
    """This is used to parse xml to dict. Ignores useless informations
    and empty values and parses the list to a consolidated list with
    individuals and entities.
    """
    # the postprocessor starts at the deepest value
    
    # save the type in the dict, because later the keys will be removed
    # and set empty second and third names and alias, if not exists
    if key == "ENTITY" or key == "INDIVIDUAL":
        value["TYPE"] = key
        value["SECOND_NAME"] = value.get("SECOND_NAME", "")
        value["THIRD_NAME"] = value.get("THIRD_NAME", "")
        value["ALIAS"] = value.get("ALIAS", [])
        return key, value

    # unify the entity list and the individual list
    if key == "CONSOLIDATED_LIST":
        value = value["ENTITIES"]["ENTITY"] + value["INDIVIDUALS"]["INDIVIDUAL"]
        return "LIST", value

    # unify INDIVIDUAL_ALIAS and ENTITY_ALIAS and ensure that the name is a 
    # string and not a dict and remove emtpy names
    if "_ALIAS" in key:
        key = "ALIAS"
        if isinstance(value, dict):
            value = value["ALIAS_NAME"]
        if value == "":
            return

    # if no value is given, list it with empty value
    if value == None:
        return key, ""

    return key, value

def download(data_folder):
    """Downloads the xml file using the functions download method."""
    logger.info("Starting downloading UN files.")
    print("Downloading UN file...")
    functions.create_folder("un", data_folder=data_folder)

    # download and save the xml
    url = "https://scsanctions.un.org/resources/xml/en/consolidated.xml"
    succ = functions.download_file(url, filename, data_folder)

    if succ:
        print("File successfully downloaded.")
        logger.info("File successfully downloaded to %s" % filename)
    return succ

