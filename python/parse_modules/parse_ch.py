# download and parse sanction list(s) from Switzerland
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import json
import xml
import xmltodict
import os

import functions
import logger

"""
Publisher information:
Switzerland offers a searching tool, that can be used to generate an
xlsx file with all 'results' (all sanctioned individuals/entities)
Unzips the xlsx file to get a xml file with the content and another xml
file with the map between the content.
"""

def parse(data_folder):
    """Parses two xml files, to generate the dictonaries containing
    sanctioned individuals and entities.
    """
    logger.info("Start parsing Switzerland")

    # check if file exits
    filename_data = os.path.join(data_folder, "ch", "xl", "sharedStrings.xml")
    filename_order = os.path.join(data_folder, "ch", "xl", "worksheets",
                                  "sheet1.xml")
    logger.debug("Checking the files %s and %s" % (filename_data, filename_order))
    for filename in [filename_data, filename_order]:
        if not functions.file_exists(filename):
            print("WARNING: Switzerland skipped because file(s) are missing. "
                  "Please download it again.")
            logger.warning("Switzerland skipped, the file %s does not exist."
                            % filename)
            return {}, {}, {}
    logger.debug("Files exist")

    print("Parsing Switzerland files...")
    individuals = {}
    entities = {}
    mixed = {}
    
    r_data = functions.open_file(filename_data)
    if r_data == None:
        return

    r_order = functions.open_file(filename_order)
    if r_order == None:
        return

    try:
        d_data = xmltodict.parse(r_data.read(), xml_attribs=False)
        d_order = xmltodict.parse(r_order.read(), xml_attribs=False)
    except xml.parsers.expat.ExpatError as e:
        msg = ("The file of the CH sanctions has an incorrect format. "
               "Please download the file again and try again.")
        logger.error(msg, error=e)
        return {}, {}, {}
    finally:
        r_data.close()
        r_order.close()

    # parse order
    rows = list(filter(None, d_order["worksheet"]["sheetData"]["row"]))
    d_order = []
    for row in rows:
        order_row = []  # number of row = number of person
        for i in range(len(row["c"])):
            if not row["c"][i] == None and "v" in row["c"][i]:
                order_row += [int(row["c"][i]["v"])]
            else:
                order_row += [None]
        d_order += [order_row]

    # get keys
    l_data = [item["t"] for item in d_data["sst"]["si"]]
    keys = []
    for nr in d_order[0]:
        keys += [l_data[nr]]

    try:
        nr_type = keys.index("Target type")
        nr_ref = keys.index("SSID")
        nr_name = keys.index("Name")
        nr_alias_1 = keys.index("Spelling variants")
        nr_alias_2 = keys.index("Good quality a.k.a.")
        #nr_address = keys.index("Address")
    except ValueError as e:
        msg = ("The structure of the Switzerland sanctions has changed. "
               "At least one key is no longer valid or no longer present.")
        logger.error(msg, error=e)
        return {}, {}, {}

    # parse data
    nr = 0
    for information_set in d_order[1:]:
        d = functions.get_empty_dict()
        if information_set[nr_name] is not None and \
                l_data[information_set[nr_name]] is not None:
            d["main name"] = l_data[information_set[nr_name]]
            d["names"] += [l_data[information_set[nr_name]]]

        if information_set[nr_alias_1] is not None and \
                l_data[information_set[nr_alias_1]] is not None:
            d["names"] += l_data[information_set[nr_alias_1]].split("\n")

        if information_set[nr_alias_2] is not None and \
                l_data[information_set[nr_alias_2]] is not None:
            d["names"] += l_data[information_set[nr_alias_2]].split("\n")

        if information_set[nr_ref] is not None and \
                l_data[information_set[nr_ref]] is not None:
            d["ref id"] = l_data[information_set[nr_ref]]

        d["link"] = "https://www.sesam.search.admin.ch/"

        if information_set[nr_type] is not None and \
                l_data[information_set[nr_type]] is not None:
            ty = l_data[information_set[nr_type]]
        else:
            ty = None

        key = "CH.%03d" % nr
        nr += 1
        if ty == "ORGANISATION":
            entities[key] = d
        elif ty == "PERSON":
            individuals[key] = d
        elif ty == "SCHIFF":
            pass
        else:
            mixed[key] = d

    print("File successfully parsed:")
    logger.debug("Switzerland files parsed successfully with %d individuals, "
                 "%s entities and %d mixed." % (len(individuals), len(entities),
                   len(mixed)))
    return individuals, entities, mixed

def download(data_folder):
    """Downloads the xml file using the functions download method."""
    logger.info("Start downloading CH files.")
    print("Downloading Switzerland file...")
    functions.create_folder("ch", data_folder=data_folder)

    filename = "switzerland.xlsx"
    path = os.path.join(data_folder, "ch")
    url = ("https://www.sesam.search.admin.ch/sesam-search-web/pages/"
           "search.xhtml?action=generateExcelAction")

    succ = functions.download_and_unzip_excel(url, filename, path)

    if succ:
        print("File successfully downloaded.")
        logger.info("File successfully downloaded to %s" % filename)
    return succ
