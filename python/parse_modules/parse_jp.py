# download and parse sanction list(s) from Japan
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import tabula
import os

import functions
import logger

filename = os.path.join("jp", "japan.pdf")

"""
Publisher information:
Japan offers a pdf file with all sanctioned entities.
No persons are sanctioned.

There is a page with a static link pointing to a page with information about
the current sanctions list. On this page is a link to the pdf file.
"""

def parse(data_folder):
    """Parses the pdf file using tabula."""
    logger.info("Start parsing Japan")

    full_filename = os.path.join(data_folder, filename)
    if not functions.file_exists(full_filename):
        print("WARNING: Japan skipped because file(s) are missing. "
              "Please download again.")
        logger.warning("Japan skipped, the file %s does not exist." % filename)
        return {}, {}, {}

    print("Parsing Japan file...")
    cols = ["No.", "Country", "Name", "AKA", "type"]
    nr_no = 0
    nr_name = 2

    tables = tabula.read_pdf(full_filename, pages="all", lattice=True)
    title_lines = []

    entities = {}

    for t in tables:  # t is one page
        # the first line is always interpreted as the title line
        # However, this is only correct for the first page, for all further
        # pages the title line (=t.colums) contains an entry
        title_lines += [t.columns]

        if len(t.columns) == 5:
            t.columns = ["No.", "Country", "Name", "AKA", "type"]
        elif len(t.columns) == 6:
            t.columns = ["No.", "Country", "Name", "AKA", "remove", "type"]
            del(t["remove"])
        else:
            msg = "Unexpected length of columns in pdf file"
            logger.error(msg)
            continue

        for index, row in t.iterrows():
            key = "JP.%03d" % int(row["No."])
            d = functions.get_empty_dict()
            d["main name"] = row["Name"]
            d["ref id"] = row["No."]
            d["link"] = "See about_publishers.md for general Japan links"
            d["names"] = [d["main name"]]
            if isinstance(row["AKA"], str):
                names = row["AKA"].replace("・", "")
                d["names"] += names.split("\r")
            entities[key] = d

    return {}, entities, {}

def _extract_link(filename, post=""):
    """Searchs for a link in a line containing "The End User List<post>".
    Returns the link or an empty string.
    """
    f = functions.open_file(filename)
    if f == None:  # file not found
        return ""

    res = ""
    for line in f.readlines():
        if "The End User List"+post in line:
            try:
                index_start = line.index("<a href=") + 9
                index_end = line.index(">", index_start) - 1
                res = line[index_start:index_end]
                break
            except  ValueError:  # substring not found
                continue
    f.close()
    return res

def download(data_folder):
    """The Japan Ministry of Economy, Trade and Industry (METI)
    offers an pdf-file with a table.
    Parse the PDF using the tabular module.
    """
    logger.info("Start downloading JP files.")
    print("Downloading JP files...")
    functions.create_folder("jp", data_folder=data_folder)

    # link to the latest overview side
    overview_link = "https://www.meti.go.jp/policy/anpo/englishpage.html"
    filename_ove = os.path.join("jp", "overview.html")
    succ = functions.download_file(overview_link, filename_ove, data_folder)
    if succ == False:
        return False

    # full (absolute) link
    review_link = _extract_link(os.path.join(data_folder, filename_ove))
    if review_link == "":
        msg = "The actual Japan sanctions file can not be found."
        logger.error(msg)
        return False
    logger.debug("Japan latest review link: %s" % review_link)

    # link to the latest file
    filename_rev = os.path.join("jp", "review.html")
    succ = functions.download_file(review_link, filename_rev, data_folder)
    if succ == False:
        return False

    link_pdf_tail = _extract_link(os.path.join(data_folder, filename_rev),
                             post=" (PDF") # relative link
    if link_pdf_tail == "":
        msg = "The actual Japan sanctions file can not be found."
        logger.error(msg)
        return False

    basic_link = os.path.dirname(review_link)
    link_pdf = os.path.join(basic_link, link_pdf_tail)
    logger.debug("Japan link to pdf file: %s" % link_pdf)

    succ = functions.download_file(link_pdf, filename, data_folder)

    if succ:
        print("File successfully downloaded.")
        logger.info("File successfully downloaded to %s" % filename)
    return succ
