# check if all modules are installed
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import logger
msg = "The module '{0}' is not installed."
n = 0

# use different try-blocks with redundant code to print more than one missing
# module

# tabula
try:
    import tabula
except ModuleNotFoundError as e:
    logger.error(msg.format(e.name))
    n += 1

# xmltodict
try:
    import xmltodict
except ModuleNotFoundError as e:
    logger.error(msg.format(e.name))
    n += 1

# zipfile
try:
    import zipfile
except ModuleNotFoundError as e:
    logger.error(msg.format(e.name))
    n += 1

# requests
try:
    import requests
except ModuleNotFoundError as e:
    logger.error(msg.format(e.name))
    n += 1

if n > 0:
    print("\nPlease install the %s missing modules with pip3 or apt (see "
          "README) and restart the program." % n)
    exit(2)
