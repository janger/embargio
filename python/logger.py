# logging
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import logging
import datetime
import subprocess
import shlex
import os

color = True
try:
    import colorama
except Exception:
    color = False

# create log folder (can't use functions.create_folder, it uses logger)
if not os.path.exists("log"):
    os.mkdir("log")

filepath = os.path.join("log", "log-%Y-%m-%d")
logfile = datetime.date.today().strftime(filepath)
FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
logging.basicConfig(level=logging.DEBUG, format=FORMAT, filename=logfile)

logging.info("\n=================== Start ==================\n")

def debug(msg):
    """Logs a debug message."""
    logging.debug(msg)

def info(msg):
    """Logs an info message."""
    logging.info(msg)

def warning(msg, print_msg=False):
    """Logs a warning message.
    If print_msg=True, then it also prints the message"""
    logging.warning(msg)
    if print_msg:
        print("WARNING: %s" % msg)

def error(msg, error=None):
    """Logs and prints an error message.
    If the color module is present, the text is printed in red.
    If the error is given, also logs the error.
    """
    if color:
        print(colorama.Fore.RED + "ERROR: %s" % msg)
        print(colorama.Style.RESET_ALL)
    else:
        print("ERROR: %s" % msg)
    logging.error(msg)
    if error:
        logging.error(error)

def fatal(msg):
    """Logs and prints a fatal message.
    If the color module is present, the text is printed in red.
    Then exits the program with exit code 1.
    """
    if color:
        print(colorama.Fore.RED + "ERROR: %s" % msg)
        print(colorama.Style.RESET_ALL)
    else:
        print("ERROR: %s" % msg)
    logging.critical(msg)
    exit(1)
