# Class to handle different data sources
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import json

import functions
import logger
import parse_modules as modules
from input_type import InputType

class SourceData:
    """Creates a data set with the data of the given publishers.
    If use_local_files=False, downloaded all needed files.
    Parses and downloads (if needed) the files using the parse
    modules.
    """
    def __init__(self, conf):
        logger.debug("Start SourceData")
        self.individuals = {}
        self.entities = {}
        self.mixed = {}
        self.data_folder = conf.save_download_to
        self.use_local_files = conf.use_local_files
        self.lengths = []

        try:
            for publisher in conf.selected_publishers:
                self._add(publisher)
                logger.debug("Added '%s'" % publisher)
        except KeyboardInterrupt:
            logger.info("User has aborted the loading of the data with a "
                        "KeyBoardInterrupt")
            print("User has aborted the loading of the data")
            exit(1)

        self.print_length_of_data_sets()

    def format_data(self, inputtype):
        """Loads the needed source data, the sanctioned persons/entities.
        The data files should contain the data in the correct format,
        otherwise throws exception.
        """
        data = {}

        # use persons
        if inputtype == InputType.INDIVIDUALS.value or \
                inputtype == InputType.MIXED.value:
            data.update(self.individuals)

        # use entities
        if inputtype == InputType.ENTITIES.value or \
                inputtype == InputType.MIXED.value:
            data.update(self.entities)

        # use always mixed
        data.update(self.mixed)
        return data

    def _add(self, publisher):
        """Downloads and parses the sanctions list, given by the short form
        of the publisher. Adds the parsed dicts to the global dicts.

        If use_local_files is True, uses the local files (from latest download)
        instead of downloading the actual list.
        """
        module = self._get_module_by_publisher(publisher)
        download_succ = True
        if not self.use_local_files:
            download_succ = module.download(self.data_folder)

        if download_succ == False:
            return

        try:
            publisher_individuals, publisher_entities, publisher_mixed = \
                                                module.parse(self.data_folder)
            n_individuals = len(publisher_individuals)
            n_entities = len(publisher_entities)
            n_mixed = len(publisher_mixed)
            self.lengths += [(n_individuals, n_entities, n_mixed)]
            print("  individuals: %d" % n_individuals)
            print("  entities   : %d" % n_entities)
            print("  mixed      : %d" % n_mixed)
            print("")
            logger.debug("Parsed list for '%s' with: %s individuals, %s "
                         "entities and %s mixed." % (publisher,
                            len(publisher_individuals), len(publisher_entities),
                            len(publisher_mixed)))
        except UnicodeError as e:
            msg = ("The downloaded files from %s has an incorrect "
                   "encoding. Please download it again and try again."
                    % publisher)
            logger.error(msg, e)
            return

        self.individuals.update(publisher_individuals)
        self.entities.update(publisher_entities)
        self.mixed.update(publisher_mixed)

    def _get_module_by_publisher(self, publisher):
        """Maps the publishers and the parse modules"""
        publisher = publisher.lower()
        if publisher == "united states of america":
            return modules.parse_us
        if publisher == "european union":
            return modules.parse_eu
        if publisher == "united nations":
            return modules.parse_un
        if publisher == "united kingdom":
            return modules.parse_uk
        if publisher == "switzerland":
            return modules.parse_ch
        if publisher == "iranwatch":
            return modules.parse_iran_watch
        if publisher == "japan":
            return modules.parse_jp
        msg = "The publisher '%s' has no parsing module." % publisher
        logger.error(msg)
        exit(1)

    def print_length_of_data_sets(self):
        """Prints the length of the infividuals, entities and mixed of
        all publishers.
        """
        print("")
        print("total individuals: %d" % len(self.individuals))
        print("total entities   : %d" % len(self.entities))
        print("total mixed      : %d" % len(self.mixed))
        print("")

if __name__ == "__main__":
    publishers = ["Switzerland", "European Union", "IranWatch", "Japan",
                  "United Kindom", "United States of America", "United Nations"]
    sd = SourceData(publishers, use_local_files=False)
