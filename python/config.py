# config class
#
# Copyright Jule Anger 2021 <ja@sernet.de>
#
# This file is part of Embargio.
#
# Embargio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Embargio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Embargio. If not, see <http://www.gnu.org/licenses/>.
#

import functions

class Config:
    def __init__(self):
        # selected publishers
        self.selected_publishers = functions.get_all_publishers()

        # download options
        self.use_local_files = False
        self.save_download_to = "data"

        # compare options
        self.comparison_level = 5
        self.min_matching_characters = 6

        # client files
        self.clientfiles = {"individuals": None, "mixed": None,
                            "entities": None}
        self.encoding = {"individuals": "utf-8", "mixed": "utf-8",
                         "entities": "utf-8"}

    def filtered_clientfiles(self):
        """Returns a list of tupels (<clienttype>, <filename>)
        with not-empty filenames.
        """
        res = []
        for key, item in self.clientfiles.items():
            if item != None:
                res += [(key, item)]
        return res

    def __str__(self):
        #ToDo: + durch join ersetzen
        res = "Actual configuration:\n"

        res += "   Use sanction files:\n"
        for publisher in self.selected_publishers:
            res += "      - %s\n" % publisher

        res += "\n    Download options:\n"
        res += "      Use local files: %s\n" % self.use_local_files
        res += "      Save downloaded files to: %s\n" % self.save_download_to

        res += "\n    Clients:\n"
        for key, value in self.clientfiles.items():
            if value != None:
                res += "      %s: %s (%s)\n" % (key, value, self.encoding[key])

        res += "\n    Compare:\n"
        res += "      comparison level: %s\n" % self.comparison_level
        res += ("      min matching characters: %s\n"
                % self.min_matching_characters)

        return res
