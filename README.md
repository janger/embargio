# Embargio

## About Embargio

Embargio is a command line program for Linux to compare client lists with
sanctioned individuals or entities.    
The program offers seven publishers of sanction lists, you can specify which
publishers are used. The publishers are:

- European Union
- IranWatch
- Japan
- Switzerland
- United Kingdom
- United Nations
- United States of America

(see `about_publishers.md` for more details about the publishers)

As a result you will get a text file with all found matches.

The program only wants to point out possible matches.
Whether the matches are correct or just random name matches and how to deal
with the hits can be decided yourself.

## Dependencies

- python3
- python3-pkg-resources
- python3-pip (is needed for the installation of the Python modules and
provides the `pip3` command)
- default-jre

mandatory python3 modules (must be installed with `pip3`):

- xmltodict
- zipfile2
- requests
- tabula-py

optional python3 modules (must be installed with `pip3`):

- colorama

On a Linux system you can install python3, python3-pkg-resources and python3-pip
with `apt install <name>` and the python3 modules with  `pip3 install <name>`.

## HowTo

Run `./embargio.py <options>` to execute the python3 script with the
configuration file settings.

Run `./embargio.py -i clients-individuals.csv` to compare sanctioned
individuals with a file named 'clients-individuals.csv', which contains human
clients (individuals).

Run `./embargio.py -e clients-entities.csv` to compare the sanctioned
entities with a file named 'clients-entities.csv', which contains companies or
other groups (entities).

## Configuration file

Please note that values set in the configuration file, are overwritten
by values given as command line arguments.

### Section [publishers]

Choose the publishers you want use.
The default value is "Yes", so missing lines are interpreted as "Yes".

```
[publishers]
switzerland = No
european union = Yes
iranwatch = No
united kingdom = Yes
united nations = No
united states of america = Yes
```

This example will use the sanctions from the European Union, United Kingdom,
United States and the missing publisher Japan.


### Section [download]

If the option "use local files = Yes" is set, the sanction files won't be
downloaded again. Instead, the saved files will be used.
This can be helpful if you want to start the program several times in succession
to avoid waiting times.

With the option "save download to" you can specify a directory where
the downloaded files should be saven. Default is "./data/".

### Section [clients]

In this section you can specify your client files.
It can be useful, if you always want to run the program with the same file name.
If you have multiple files, take a look at the command line options, then you
don't have to change the config file for each run.

```
[clients]
individuals = 
entities = companies.csv
mixed = 
```

This example will test only the entity file "companies.csv".

### Section [compare]

There are different "comparison levels". A low level means a strong match.
All levels below the specified one are used.
For example, if you choose level 3, then level 1, level 2 and level 3
are used. The default level is 5.

- Level 1: perfect match, like John Doe as client and John Doe as sanctioned
- Level 2: the sanctioned name contains all parts of the name from the client,
like John Melchior Doe as client and John Doe as sanctioned
- Level 3: the client name contains all parts of the name from the sanctioned,
like John Doe as client and John Melchior Doe as sanctioned
- Level 4: client and sanctioned have at least two name parts in common,
like John Melchior Doe and Nickolas John Doe
- Level 5: client and sanctioned have the same surname,
like John Doe and Nickolas Doe

### Example

```
[publishers]
switzerland = No
european union = Yes
iranwatch = No
japan = Yes
united kingdom = Yes
united nations = No
united states of america = No

[download]
use local files = No
save download to = data_files

[clients]
individuals = my-clients.csv
entities = companies.csv

[compare]
comparison level = 3
```

This example compares the publishers European Union, Japan and United Kingdom
with the two client files 'my-clients.csv' and 'companies.csv'.
The sanctions list files are downloaded and stored in the folder 'data'.
The comparison level 3 is used.

## Commandline parameters

Please note that values, passed via the command line, will overwrite
values in the configuration file.

Also you can run `./embargio.py --help`.

```
Options:
  -h, --help            show this help message and exit
  -i FILE, --individual=FILE
                        client file for individuals
  -e FILE, --entities=FILE
                        client file for entities
  -m FILE, --mixed=FILE
                        client file for individuals and entities (mixed)
  -c FILE, --config=FILE
                        alternative configuration file, default is
                        'embargio.config'
```

## Todos and known bugs
- coding errors with Windows
- partly the download is very slow
